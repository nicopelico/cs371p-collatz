// ----------------------------
// projects/collatz/Collatz.c++
// Copyright (C) 2013
// Glenn P. Downing
// ----------------------------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#define cacheSize 1000000

#include "Collatz.h"

// ------------
// collatz_read
// ------------

bool collatz_read (std::istream& r, int& i, int& j) {
    r >> i;
    if (!r)
        return false;
    r >> j;
    assert(i > 0);
    assert(j > 0);
    return true;}

	
	

int myCache[cacheSize];
	
// ------------
// collatz_eval
// ------------

int collatz_eval (int i, int j) {
using namespace std;
	int maxCycleLen = 0;
	int x = i < j ? i : j;
	int y = i < j ? j : i;
	int m = y / 2;
	//using question 3 from quiz 3 (if x < m, then maxCycleLength(x, y) = maxCycleLength(m, y)
	if(x < m)
		x = y / 2;
	
	for( ;x <= y; x++){
		int numCycles = getCycleLength(x);
		myCache[x] = numCycles;		//map the number of cycles to the current number in the range
		if(numCycles > maxCycleLen)
			maxCycleLen = numCycles;
	}
	
	return maxCycleLen;}
	
// -------------
// getCycleLength
// -------------

int getCycleLength(int num){
	int tempVal = num;			//holds the intermediate values while testing the conjecture
	int numCycles = 1;			//initialize to 1. Number of cycles includes both end points of range
	//if value is not in cache
	if(myCache[num] == 0){
		while(tempVal > 1){		
			if(tempVal % 2 == 0){
				tempVal = tempVal >> 1;
				numCycles++;
			}
			else{
				tempVal = tempVal + (tempVal >> 1) + 1;		//(3n + 1)/2
				numCycles += 2;
			}
			//check if in cache to exit calculations early (if tempVal could be in cache.)
			if(tempVal < cacheSize){
				if(myCache[tempVal] != 0){
					numCycles += myCache[tempVal] - 1;
					tempVal = 1;	//assign to 1 to break out of loop
				}
			}
		}
	}
	
	//else value is in cache
	else{
		numCycles = myCache[num];
	}
	return numCycles;
}

// -------------
// collatz_print
// -------------

void collatz_print (std::ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << std::endl;}

// -------------
// collatz_solve
// -------------

void collatz_solve (std::istream& r, std::ostream& w) {
    int i;
    int j;
    while (collatz_read(r, i, j)) {
        const int v = collatz_eval(i, j);
        collatz_print(w, i, j, v);}}