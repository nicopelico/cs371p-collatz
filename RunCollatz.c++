// -------------------------------
// projects/collatz/RunCollatz.c++
// Copyright (C) 2013
// Glenn P. Downing
// -------------------------------

/*
To run the program:
    % g++ -pedantic -std=c++0x -Wall Collatz.c++ RunCollatz.c++ -o RunCollatz
    % valgrind RunCollatz < RunCollatz.in > RunCollatz.out

To configure Doxygen:
    % doxygen -g
That creates the file "Doxyfile".
Make the following edits:
    EXTRACT_ALL            = YES
    EXTRACT_PRIVATE        = YES
    EXTRACT_STATIC         = YES
    GENERATE_LATEX         = NO

To document the program:
    % doxygen Doxyfile
*/

// -------
// defines
// -------

#ifdef ONLINE_JUDGE
    #define NDEBUG
#endif

// --------
// includes
// --------

#include <iostream> // cin, cout
#include <fstream>	//ifstream, ofstream
#include "Collatz.h"

// ----
// main
// ----

int main () {
	using namespace std;
	ifstream inputFile;
	ofstream outputFile;
	
	inputFile.open("nico-RunCollatz.in");
	outputFile.open("output.out");
    
	//collatz_solve(inputFile, outputFile);
    collatz_solve(cin, cout);
	inputFile.close();
	outputFile.close();
    return 0;}